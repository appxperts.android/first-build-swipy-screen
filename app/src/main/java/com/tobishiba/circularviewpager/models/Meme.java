package com.tobishiba.circularviewpager.models;

import com.tobishiba.circularviewpager.R;

import java.util.ArrayList;
import java.util.List;

/**
 * User: Gufran Khurshid
 * Date: 18.09.14 | Time: 11:02
 */
public class Meme {
    public String mTitle;
    public int mImageResourceId;

    public static List<Meme> createSampleMemes() {
        final List<Meme> memes = new ArrayList<Meme>();
        memes.add(new Meme("is scrolling", R.drawable.screen_01));
        memes.add(new Meme("endless", R.drawable.screen_02));
        memes.add(new Meme("endless", R.drawable.screen_03));
        memes.add(new Meme("endless", R.drawable.screen_04));
        memes.add(new Meme("endless", R.drawable.screen_05));
        memes.add(new Meme("endless", R.drawable.screen_06));
        memes.add(new Meme("endless", R.drawable.screen_07));
        memes.add(new Meme("endless", R.drawable.screen_08));
        memes.add(new Meme("endless", R.drawable.screen_09));
        memes.add(new Meme("endless", R.drawable.screen_10));
        memes.add(new Meme("endless", R.drawable.screen_11));
        memes.add(new Meme("endless", R.drawable.screen_12));
        memes.add(new Meme("endless", R.drawable.screen_13));
        memes.add(new Meme("endless", R.drawable.screen_14));
        return memes;
    }

    public Meme(final String title, final int imageResourceId) {
        mTitle = title;
        mImageResourceId = imageResourceId;
    }
}
